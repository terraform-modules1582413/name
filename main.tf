resource "random_id" "appendix" {
  byte_length = 2
}

variable "prefix" {
  type        = string
  description = "maximum three-character abbreviation"
}

variable "environment" {
  type = string
}

variable "resource" {
  type = string
}

variable "name" {
  type = string
}

output "full" {
  value = "${var.prefix}-${var.environment}-${var.resource}-${random_id.appendix.hex}"
}

output "short" {
  value = "${var.prefix}-${var.environment}-${var.resource}"
}

output "pretty" {
  value = "${var.name}-${var.environment}"
}

output "domain_friendly" {
  value = "${var.name}-${var.resource}-${var.environment}"
}

output "appendix" {
  value = random_id.appendix.hex
}