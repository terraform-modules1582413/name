<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_random"></a> [random](#provider\_random) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [random_id.appendix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/id) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_environment"></a> [environment](#input\_environment) | n/a | `string` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | n/a | `string` | n/a | yes |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | maximum three-character abbreviation | `string` | n/a | yes |
| <a name="input_resource"></a> [resource](#input\_resource) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_appendix"></a> [appendix](#output\_appendix) | n/a |
| <a name="output_domain_friendly"></a> [domain\_friendly](#output\_domain\_friendly) | n/a |
| <a name="output_full"></a> [full](#output\_full) | n/a |
| <a name="output_pretty"></a> [pretty](#output\_pretty) | n/a |
| <a name="output_short"></a> [short](#output\_short) | n/a |
<!-- END_TF_DOCS -->